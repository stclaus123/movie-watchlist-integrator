package sk.common.watchlistinfodownloder;

import java.util.List;

public class Movie {
	
	private String name;
	private String originalName;
	private String Director;
	private int runtime; //runtime in mins
	private int year; //year of release
	private List<String> genres;
	private String description;
	private List<String> actors;
	private List<Rating> ratings;
}
